variable "tf_state_bucket" {
    description = " Name of S3 bucke in AWS for storing TF state"
    default = "ops-recipe-app-tf-state"
}

variable "tf_state_lock_table" {
    description = "Name of the Dynamo table fo TF state locking"
    default = "ops-recipe-api-lock" 
}

variable "project" {
    description = "Project name for tagging resources"
    default = "recipe-app-api"
}

variable "contact" {
    description = "Contact name for tagging resources"
    default = "raymund@example.com"
}
